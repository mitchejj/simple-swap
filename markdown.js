var metalsmith = require('metalsmith')
var markdown = require('metalsmith-markdown')

metalsmith(__dirname)
.destination('public/_md')
.use(markdown())
.build(function (err) {
  if (err) throw err
})