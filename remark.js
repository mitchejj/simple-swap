var metalsmith = require('metalsmith')
var remark = require('metalsmith-remark')

metalsmith(__dirname)
.destination('public/remark')
.use(remark([]))
.build(function (err) {
  if (err) throw err
})